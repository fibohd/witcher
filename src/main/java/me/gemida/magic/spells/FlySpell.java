package me.gemida.magic.spells;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class FlySpell extends Spell {

	public FlySpell() {
		super(15, new ItemStack(Material.FEATHER));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action(Player user) {
		user.setVelocity(user.getLocation().getDirection().multiply(2D));
		user.setFallDistance(0);
		
	}

}
