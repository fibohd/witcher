package me.gemida.magic.spells;

import java.util.LinkedHashSet;

public class SpellManager {
	private LinkedHashSet<Spell> spells = new LinkedHashSet<Spell>();
	
	public SpellManager() {
		spells.add(new ArrowSpell());
		spells.add(new HealSpell());
		spells.add(new FlySpell());
		spells.add(new MineSpell());
	}
	
	public LinkedHashSet<Spell> getSpells(){
		return spells;
	}
}
