package me.gemida.magic.spells;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class Spell {
	private int xpcosts;
	private ItemStack item;
	public Spell(int xpcosts, ItemStack item) {
		this.setXpcosts(xpcosts);
		this.item = item;
	}
	
	public abstract void action(Player user);

	public int getXpcosts() {
		return xpcosts;
	}
	
	public ItemStack getItem() {
		return item;
	}

	public void setXpcosts(int xpcosts) {
		this.xpcosts = xpcosts;
	}

}
