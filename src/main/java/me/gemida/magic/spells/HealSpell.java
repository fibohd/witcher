package me.gemida.magic.spells;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class HealSpell extends Spell{

	public HealSpell() {
		super(100, new ItemStack(Material.POPPY));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action(Player user) {
		user.setHealth(user.getHealthScale());
		
	}

}
