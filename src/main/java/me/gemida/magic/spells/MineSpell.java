package me.gemida.magic.spells;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MineSpell extends Spell{

	public MineSpell() {
		super(20, new ItemStack(Material.GOLDEN_PICKAXE));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action(Player user) {
		user.getTargetBlockExact(3).breakNaturally();
		
	}

}
