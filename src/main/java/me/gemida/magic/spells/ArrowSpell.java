package me.gemida.magic.spells;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArrowSpell extends Spell{

	public ArrowSpell() {
		super(10, new ItemStack(Material.ARROW));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void action(Player user) {
		Arrow arrow = user.getWorld().spawnArrow(user.getLocation().add(0, 1, 0), user.getLocation().getDirection(), 3f, 0);
		arrow.setFireTicks(Integer.MAX_VALUE);
	}
	
}
