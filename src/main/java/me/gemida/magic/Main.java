package me.gemida.magic;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.gemida.magic.commands.Hunter_CMD;
import me.gemida.magic.commands.Wand_CMD;
import me.gemida.magic.listener.BloodSwordListener;
import me.gemida.magic.listener.BloodSwordRitualListener;
import me.gemida.magic.listener.HunterBowListener;
import me.gemida.magic.listener.HunterRitualListener;
import me.gemida.magic.listener.HunterSwordListener;
import me.gemida.magic.listener.SpellListener;
import me.gemida.magic.listener.WandRitualListener;
import me.gemida.magic.spells.SpellManager;

public class Main extends JavaPlugin {
	private static Main plugin;
	private static SpellManager spellManager;
	@Override
	public void onEnable() {
		plugin = this;
		spellManager = new SpellManager();
		
		getCommand("wand").setExecutor(new Wand_CMD());
		getCommand("hunter").setExecutor(new Hunter_CMD());
		
		Bukkit.getPluginManager().registerEvents(new SpellListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new WandRitualListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new HunterSwordListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new HunterBowListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new HunterRitualListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new BloodSwordRitualListener(), plugin);
		Bukkit.getPluginManager().registerEvents(new BloodSwordListener(), plugin);
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static SpellManager getSpellManager() {
		return spellManager;
	}
}
