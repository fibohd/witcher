package me.gemida.magic.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import me.gemida.magic.Main;
import me.gemida.magic.spells.Spell;

public class SpellListener implements Listener{
	@EventHandler
	public static void onSpell(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName() != null && e.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase("�4Zauberstab")) {
				for(Spell spell:Main.getSpellManager().getSpells()) {
					if(e.getPlayer().getInventory().getItemInOffHand().getType() == spell.getItem().getType()) {
						if(spell.getXpcosts() <= e.getPlayer().getTotalExperience()) {
							spell.action(e.getPlayer());
							e.getPlayer().giveExp(spell.getXpcosts() * -1);
						}
					}
				}
			}
		}
	}
}
