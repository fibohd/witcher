package me.gemida.magic.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.gemida.magic.Main;

public class BloodSwordRitualListener implements Listener{
	
	@EventHandler
	public static void onDrop(final PlayerDropItemEvent e) {
		if(e.getItemDrop().getItemStack().getType() == Material.IRON_SWORD && e.getPlayer().hasPermission("Magier")) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
				
				public void run() {
					if(e.getItemDrop().getLocation().getBlock().getType() == Material.WATER) {
						ItemStack is = new ItemStack(Material.IRON_SWORD);
						ItemMeta im = is.getItemMeta();
						im.setDisplayName("�4Ritual Messer");
						is.setItemMeta(im);
						e.getItemDrop().getWorld().dropItem(e.getItemDrop().getLocation(), is);
						e.getItemDrop().getLocation().getBlock().setType(Material.AIR);
						e.getItemDrop().remove();
						
					}
					
				}
			}, 20);
		}
	}

}
