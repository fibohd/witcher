package me.gemida.magic.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.gemida.magic.Main;

public class HunterRitualListener implements Listener{
	
	@EventHandler
	public static void onDrop(final PlayerDropItemEvent e) {
		if(e.getItemDrop().getItemStack().getType() == Material.ROTTEN_FLESH && e.getPlayer().hasPermission("Hunter")) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
				
				public void run() {
					if(e.getItemDrop().getLocation().getBlock().getType() == Material.WATER) {
						e.getItemDrop().getLocation().getBlock().setType(Material.AIR);
						ItemStack is = new ItemStack(Material.DIAMOND_SWORD);
						ItemMeta im = is.getItemMeta();
						im.setDisplayName("�5Hexen J�ger");
						im.addEnchant(Enchantment.MENDING, 3, true);
						is.setItemMeta(im);
						Zombie z = (Zombie) e.getItemDrop().getWorld().spawnEntity(e.getItemDrop().getLocation().add(0, 5, 0), EntityType.ZOMBIE);
						z.getEquipment().setItemInMainHand(is);
						z.getEquipment().setItemInMainHandDropChance(0.5f);
						z.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						e.getItemDrop().remove();
					}
					
				}
			}, 20);
		}else if(e.getItemDrop().getItemStack().getType() == Material.BONE && e.getPlayer().hasPermission("Hunter")) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
				
				public void run() {
					if(e.getItemDrop().getLocation().getBlock().getType() == Material.WATER) {
						e.getItemDrop().getLocation().getBlock().setType(Material.AIR);
						ItemStack is1 = new ItemStack(Material.BOW);
						ItemMeta im1 = is1.getItemMeta();
						im1.setDisplayName("�5Bogen des Todes");
						im1.addEnchant(Enchantment.MENDING, 3, true);
						im1.addEnchant(Enchantment.ARROW_FIRE, 1, true);
						im1.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
						is1.setItemMeta(im1);
						Skeleton z = (Skeleton) e.getItemDrop().getWorld().spawnEntity(e.getItemDrop().getLocation().add(0, 5, 0), EntityType.SKELETON);
						z.getEquipment().setItemInMainHand(is1);
						z.getEquipment().setItemInMainHandDropChance(0.5f);
						z.getEquipment().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
						z.getEquipment().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
						z.getEquipment().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
						z.getEquipment().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
						e.getItemDrop().remove();					
					}
					
				}
			}, 20);
		}
	}
	
}
