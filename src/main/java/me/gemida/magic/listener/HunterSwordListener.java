package me.gemida.magic.listener;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HunterSwordListener implements Listener{
	@EventHandler
	public static void onHit(EntityDamageByEntityEvent e) {
		if(e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
			Player damager = (Player)e.getDamager();
			LivingEntity entity = (LivingEntity) e.getEntity();
			if(damager.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase("�5Hexen J�ger")) {
				entity.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*30, 3));
				if(entity instanceof Player) {
					((Player) entity).giveExp(-10);
				}
			}
		}
	}
}
