package me.gemida.magic.listener;

import org.bukkit.Color;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HunterBowListener implements Listener{
	@EventHandler
	public static void onShootBow(EntityShootBowEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player)e.getEntity();
			if(e.getBow().getItemMeta().getDisplayName().equalsIgnoreCase("�5Bogen des Todes")) {
				if(e.getProjectile() instanceof Arrow) {
					Arrow arrow = (Arrow)e.getProjectile();
					arrow.setCritical(true);
					arrow.addCustomEffect(new PotionEffect(PotionEffectType.SLOW, 20*30, 3), false);
					arrow.addCustomEffect(new PotionEffect(PotionEffectType.WITHER, 20*10, 1), false);
					arrow.setColor(Color.BLACK);
					arrow.setCustomName("DeathBow");
				}
			}
		}
	}
	
	@EventHandler
	public static void onHit(ProjectileHitEvent e) {
		if(e.getEntity().getCustomName() != null && e.getEntity().getCustomName().equalsIgnoreCase("DeathBow") ) {
			if(e.getHitEntity() instanceof Player) {
				((Player)e.getHitEntity()).giveExp(-20);
			}
			e.getEntity().remove();
			
		}
	}
	
	
}
