package me.gemida.magic.listener;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.gemida.magic.Main;

public class WandRitualListener implements Listener {
	@EventHandler
	public static void onWandCreate(final PlayerDropItemEvent e) {
		if(e.getItemDrop().getItemStack().getItemMeta().getDisplayName().equalsIgnoreCase("Magicum") && e.getPlayer().hasPermission("Magier")) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
				
				public void run() {
					if(e.getItemDrop().getLocation().getBlock().getType() == Material.WATER) {
						e.getItemDrop().getLocation().getBlock().setType(Material.AIR);
						e.getItemDrop().getLocation().getBlock().setType(Material.LAVA);
						ItemStack is = new ItemStack(Material.STICK);
						ItemMeta im = is.getItemMeta();
						im.setDisplayName("�4Zauberstab");
						is.setItemMeta(im);
						Item i = e.getItemDrop().getLocation().getWorld().dropItem(e.getItemDrop().getLocation().add(0, 10, 0), is);
						e.getItemDrop().getLocation().getWorld().playEffect(e.getItemDrop().getLocation(), Effect.DRAGON_BREATH, 1, 3);
						e.getItemDrop().remove();
						
					}
					
				}
			}, 20);
		}
	}
}
