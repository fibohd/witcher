package me.gemida.magic.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BloodSwordListener implements Listener{
	@EventHandler
	public static void onInteract(PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_AIR) {
			if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("�4Ritual Messer")) {
				e.getPlayer().setHealth(e.getPlayer().getHealth() - 1D);
				e.getPlayer().giveExp(3);
			}
		}
	}
	
	@EventHandler
	public static void onKill(EntityDeathEvent e) {
		if(e.getEntity().getKiller().getInventory().getItemInMainHand().getItemMeta().getDisplayName() != null && e.getEntity().getKiller() != null && e.getEntity().getKiller().getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase("�4Ritual Messer")) {
			e.setDroppedExp(100);
		}
	}
}
