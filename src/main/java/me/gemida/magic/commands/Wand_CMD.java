package me.gemida.magic.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Wand_CMD implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player)sender;
		if(command.getName().equalsIgnoreCase("wand")) {
			if(p.hasPermission("wand")) {
				ItemStack is = new ItemStack(Material.STICK);
				ItemMeta im = is.getItemMeta();
				im.setDisplayName("�4Zauberstab");
				is.setItemMeta(im);
				p.getInventory().addItem(is);
			}
			
		}
		return true;
	}

}
