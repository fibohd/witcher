package me.gemida.magic.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Hunter_CMD implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player)sender;
		if(command.getName().equalsIgnoreCase("hunter")) {
			ItemStack is = new ItemStack(Material.IRON_SWORD);
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("�5Hexen J�ger");
			is.setItemMeta(im);
			
			ItemStack is1 = new ItemStack(Material.BOW);
			ItemMeta im1 = is.getItemMeta();
			im1.setDisplayName("�5Bogen des Todes");
			is1.setItemMeta(im1);
			p.getInventory().addItem(is, is1);
			
		}
		return true;
	}

}
